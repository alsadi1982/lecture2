package sbp.branching;

import sbp.common.Utils;

public class MyCycles
{
    private final Utils utils;

    public MyCycles(Utils utils)
    {
        this.utils = utils;
    }

    /**
     * Необходимо написать реализацию метода с использованием for()
     +     * Метод должен вызвать Utils#utilFunc1() на каждой итерации
     +     * Реализация Utils#utilFunc1() неизвестна
     +     * Должна присутствовать проверка возврщаемого значения от Utils#utilFunc1()
     * Результат проверяется через unit-test (все тесты должны выполниться успешно)
     * @param iterations - количество итераций
     * @param str - строка для вывода через утилиту {@link Utils}
     */
    public void cycleForExample(int iterations, String str)
    {
        /**
         * Заходим в цикл for
         * В проверке условия выполнения цикла вызываю Utils#utilFunc1() и сразу проверяю возвращаемое значение
            * Если возвращаемое значение true - переходим на следующую итерацию
            * Если возвращаемое значение false - выходим из цикла
         */
        for (int i = 0; i < iterations && this.utils.utilFunc1(str); i++){

        }

    }

    /**
     * Необходимо написать реализацию метода с использованием while()
     +     * Метод должен вызвать Utils#utilFunc1() на каждой итерации
     +     * Реализация Utils#utilFunc1() неизвестна
     +     * Должна присутствовать проверка возврщаемого значения от Utils#utilFunc1()
     * Результат проверяется через unit-test (все тесты должны выполниться успешно)
     * @param iterations - количество итераций
     * @param str - строка для вывода через утилиту {@link Utils}
     */
    public void cycleWhileExample(int iterations, String str)
    {
        /**
         * Создаем переменную i - счетчик итераций
         * Заходим в цикл while
            * В проверке условия выполнения цикла вызываю Utils#utilFunc1() и сразу проверяю возвращаемое значение
                * Если возвращаемое значение true - переходим на следующую итерацию и декремируем счётчик итераций
                * Если возвращаемое значение false - выходим из цикла

         */
        int i = 0;

        while(i < iterations && utils.utilFunc1(str)){
            i++;
        }
    }

    /**
     * Необходимо написать реализацию метода с использованием while()
     -     * Метод должен вызвать Utils#utilFunc1() на каждой итерации
     -     * Реализация Utils#utilFunc1() неизвестна
     +     * Метод должен вызвать Utils#utilFunc1() на каждой итерации
     +     * Реализация Utils#utilFunc1() неизвестна
     * Должна присутствовать проверка возврщаемого значения
     * Результат проверяется через unit-test (все тесты должны выполниться успешно)
     * @param from - начальное значение итератора
     * @param to - конечное значение итератора
     * @param str - строка для вывода через утилиту {@link Utils}
     */
    public void cycleDoWhileExample(int from, int to, String str)
    {
        /**
         * Заходим в цикл do while
            * В if вызываем Utils#utilFunc1() и сразу проверяю возвращаемое значение
                * Если возвращаемое значение true - продолжаем выполнение цикла и сравниваем параметры from и to
                    * Если from < to - декрементирую from
                    * Если from > to - инкрементирую from
                * Если возвращаемое значение false - вызываем оператор brake и выходим из цикла

         */
        do {
            if (!utils.utilFunc1(str)){
                break;
            }

            if (from < to){
                from++;
            }else if (from > to){
                from--;
            }

        }while (from != to);
    }
}
