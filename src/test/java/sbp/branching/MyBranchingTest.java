package sbp.branching;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Assertions;
import org.mockito.Mockito;
import sbp.common.Utils;

import static org.mockito.ArgumentMatchers.anyString;

/**
 * Класс MyBranchingTest содержит тесты к {@link MyBranching}
 */

public class MyBranchingTest {

    /**
     * Проверяем выполнения функции MyBranching#maxInt(int, int)
     * Проверяем сценарий при котором функции utils.utilFunc2 и utilsMock.utilFunc1(String) возвращают false
     */

    @Test
    public void maxInt_Success_Test (){

        Utils utilsMock = Mockito.mock(Utils.class);

        Mockito.when(utilsMock.utilFunc2()).thenReturn(false);
        Mockito.when(utilsMock.utilFunc1(anyString())).thenReturn(false);

        MyBranching myBranching = new MyBranching(utilsMock);

        myBranching.maxInt(1, 2);

        Mockito.verify(utilsMock, Mockito.times(1)).utilFunc2();
        Mockito.verify(utilsMock, Mockito.never()).utilFunc1(anyString());

        int i1 = 2;
        int i2 = 3;
        Assertions.assertEquals(i2, myBranching.maxInt(i1, i2));
         i1 = 5;
         i2 = 4;
        Assertions.assertEquals(i1, myBranching.maxInt(i1, i2));
         i1 = 10;
         i2 = 10;
        Assertions.assertEquals(i1, myBranching.maxInt(i1, i2));

    }

    /**
     * Проверяем выполнения функции MyBranching#maxInt(int, int)
     * Проверяем сценарий при котором функции utils.utilFunc2 возвращет true, а utilsMock.utilFunc1(String) возвращают folse
     */

    @Test
    public void maxInt_Fail1_Test (){

        Utils utilsMock = Mockito.mock(Utils.class);

        Mockito.when(utilsMock.utilFunc2()).thenReturn(true);
        Mockito.when(utilsMock.utilFunc1(anyString())).thenReturn(false);

        MyBranching myBranching = new MyBranching(utilsMock);
        int i1 = 2;
        int i2 = 1;
        myBranching.maxInt(i1, i2);

        Mockito.verify(utilsMock, Mockito.times(1)).utilFunc2();
        Mockito.verify(utilsMock, Mockito.times(i2)).utilFunc1(anyString());


        Assertions.assertEquals(0, myBranching.maxInt(i1, i2));
         i1 = 5;
         i2 = 4;
        Assertions.assertEquals(0, myBranching.maxInt(i1, i2));
         i1 = 10;
         i2 = 10;
        Assertions.assertEquals(0, myBranching.maxInt(i1, i2));

    }

    /**
     * Проверяем выполнения функции MyBranching#maxInt(int, int)
     * Проверяем сценарий при котором функции utils.utilFunc2 и utilsMock.utilFunc1(String) возвращают true
     */

    @Test
    public void maxInt_Fail2_Test (){

        Utils utilsMock = Mockito.mock(Utils.class);

        Mockito.when(utilsMock.utilFunc2()).thenReturn(true);
        Mockito.when(utilsMock.utilFunc1(anyString())).thenReturn(true);

        MyBranching myBranching = new MyBranching(utilsMock);
        int i1 = 2;
        int i2 = 1;
        myBranching.maxInt(i1, i2);

        Mockito.verify(utilsMock, Mockito.times(1)).utilFunc2();
        Mockito.verify(utilsMock, Mockito.times(1)).utilFunc1(anyString());


        Assertions.assertEquals(0, myBranching.maxInt(i1, i2));
         i1 = 5;
         i2 = 4;
        Assertions.assertEquals(0, myBranching.maxInt(i1, i2));
         i1 = 10;
         i2 = 10;
        Assertions.assertEquals(0, myBranching.maxInt(i1, i2));

    }

    /**
     * Проверяем выполнение функции MyBranching#ifElseExample()
     * Проверяем успешный сценарий выполнения utils.utilFunc2
     */

    @Test
    public void ifElseExample_Success_Test(){

        Utils utilsMock = Mockito.mock(Utils.class);
        Mockito.when(utilsMock.utilFunc2()).thenReturn(true);

        MyBranching myBranching = new MyBranching(utilsMock);

        Assertions.assertTrue(myBranching.ifElseExample());

        Mockito.verify(utilsMock, Mockito.times(1)).utilFunc2();
        Mockito.verify(utilsMock, Mockito.never()).utilFunc1(anyString());

    }
/**
     * Проверяем выполнение функции MyBranching#ifElseExample()
     * Проверяем неуспешный сценарий выполнения utils.utilFunc2
     */

    @Test
    public void ifElseExample_Fail_Test(){

        Utils utilsMock = Mockito.mock(Utils.class);
        Mockito.when(utilsMock.utilFunc2()).thenReturn(false);

        MyBranching myBranching = new MyBranching(utilsMock);

        Assertions.assertFalse(myBranching.ifElseExample());

        Mockito.verify(utilsMock, Mockito.times(1)).utilFunc2();
        Mockito.verify(utilsMock, Mockito.never()).utilFunc1(anyString());

    }


    /**
     * Проверяем выполнение функции MyBranching#switchExample(int)
     * Проверяем выполнение Utils#utilFunc1 и Utils#utilFunc2 при входящем i = 1
     */

    @Test
    public void switchExample_1_Test(){

        Utils utilsMock = Mockito.mock(Utils.class);
        MyBranching myBranching = new MyBranching(utilsMock);

        int i = 1;
        myBranching.switchExample(i);
        Mockito.verify(utilsMock, Mockito.times(1)).utilFunc1(anyString());
        Mockito.verify(utilsMock, Mockito.times(1)).utilFunc2();

    }

    /**
        * Проверяем выполнение функции MyBranching#switchExample(int)
        * Проверяем выполнение Utils#utilFunc1 и Utils#utilFunc2 при входящем i = 2
     */

    @Test
    public void switchExample_2_Test(){

        Utils utilsMock = Mockito.mock(Utils.class);
        MyBranching myBranching = new MyBranching(utilsMock);

        int i = 2;
        myBranching.switchExample(i);
        Mockito.verify(utilsMock, Mockito.never()).utilFunc1(anyString());
        Mockito.verify(utilsMock, Mockito.times(1)).utilFunc2();

    }

    /**
        * Проверяем выполнение функции MyBranching#switchExample(int)
        * Проверяем выполнение Utils#utilFunc1 и Utils#utilFunc2 при входящем i = 0
          * Проверяем сценарий когда когда Utils#utilFunc2 возвращает true
     */

    @Test
    public void switchExample_0True_Test(){

        Utils utilsMock = Mockito.mock(Utils.class);
        Mockito.when(utilsMock.utilFunc2()).thenReturn(true);

        MyBranching myBranching = new MyBranching(utilsMock);

        int i = 0;
        myBranching.switchExample(i);
        Mockito.verify(utilsMock, Mockito.times(1)).utilFunc2();
        Mockito.verify(utilsMock, Mockito.times(1)).utilFunc1(anyString());

    }

    /**
        * Проверяем выполнение функции MyBranching#switchExample(int)
        * Проверяем выполнение Utils#utilFunc1 и Utils#utilFunc2 при входящем i = 0
          * Проверяем сценарий когда когда Utils#utilFunc2 возвращает false
     */

    @Test
    public void switchExample_0False_Test(){

        Utils utilsMock = Mockito.mock(Utils.class);
        Mockito.when(utilsMock.utilFunc2()).thenReturn(false);

        MyBranching myBranching = new MyBranching(utilsMock);

        int i = 0;
        myBranching.switchExample(i);
        Mockito.verify(utilsMock, Mockito.times(1)).utilFunc2();
        Mockito.verify(utilsMock, Mockito.never()).utilFunc1(anyString());

    }

}
